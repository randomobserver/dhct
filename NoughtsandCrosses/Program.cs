﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoughtsandCrosses
{
    class Program
    {
        static void Main(string[] args)
        {
            Engine.Implementations.Board board = new Engine.Implementations.Board();
            board.Play();
        }
    }
}
