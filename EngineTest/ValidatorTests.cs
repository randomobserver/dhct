﻿using Engine;
using Engine.Implementations;
using Engine.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineTest
{
    [TestClass]
    public class ValidatorTests
    {
        [TestMethod]
        public void Coordinates_OutSide_Of_The_Board()
        {
            //Arrange
            IValidator validator = new Validator();
            //Act
            bool result = validator.LegalStep(-1, -1, CellType.Nought);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Coordinates_InSide_Of_The_Board_And_Right_Cell_Type()
        {
            //Arrange
            IValidator validator = new Validator();
            //Act
            bool result = validator.LegalStep(1, 1, CellType.Empty);
            //Assert
            Assert.IsTrue(result);
        } 
    }
}
