﻿using Engine.Interfaces;
using Engine.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace EngineTest
{
    [TestClass]
    public class HorizontalCheckerTests
    {
        [TestMethod]
        public void Add_Cross_Results_In_A_Won_Row()
        {
            //Arrange
            IDictionary<int, List<ICell>> Cells = new Dictionary<int, List<ICell>>();
            ICell cell1 = new Cell();
            cell1.x = 1;
            cell1.y = 0;
            cell1.cellType = Engine.CellType.Cross;

            ICell cell3 = new Cell();
            cell3.x = 1;
            cell3.y = 2;
            cell3.cellType = Engine.CellType.Cross;

            List<ICell> row2 = new List<ICell>();
            row2.Add(cell1);
            row2.Add(cell3);

            Cells.Add(1, row2);
            IDirectionChecker horizontalChecker = new HorizontalChecker();
            //Act
            bool result = horizontalChecker.DirectionWon(Cells, Engine.CellType.Cross, 1, 1);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Add_Cross_Results_In_A_Lost_Row()
        {
            //Arrange
            IDictionary<int, List<ICell>> Cells = new Dictionary<int, List<ICell>>();
            ICell cell1 = new Cell();
            cell1.x = 1;
            cell1.y = 0;
            cell1.cellType = Engine.CellType.Cross;

            ICell cell3 = new Cell();
            cell3.x = 1;
            cell3.y = 2;
            cell3.cellType = Engine.CellType.Nought;

            List<ICell> row2 = new List<ICell>();
            row2.Add(cell1);
            row2.Add(cell3);

            Cells.Add(1, row2);
            IDirectionChecker horizontalChecker = new HorizontalChecker();
            //Act
            bool result = horizontalChecker.DirectionWon(Cells, Engine.CellType.Cross, 1, 1);
            //Assert
            Assert.IsFalse(result);
        }
    }
}
