# README #

In this coding exercise I tried to keep things as loosely coupled as possible and keep types with high cohesion.
As time was quickly passing by I had to do some sacrifices e.g. in the Board class. It has too many responsibilities.
Few tests had been added
and the whole functionality follows the rule 'Program to an interface not an implementation' and behaviour is composed out of cohesive types(most of it) so the solution is easily unittestable.
The solution is also extendable as I wrote it following the 4 OOP and SOLID principles.

# Main types #

The information is hold in the Cell type.
Whether there is a win a contact called IDirectionChecker has been added.
It has implementation for some directions but the remaining ones can easily be added.
IValidator for validating the input.
IBoard is the main type and its behaviour is composed of the created contracts.
