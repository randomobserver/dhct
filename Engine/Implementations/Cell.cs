﻿using Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Implementations
{
    public class Cell : ICell
    {
        public int x { get; set; }
        public int y { get; set; }
        public CellType cellType { get; set; }
    }
}
