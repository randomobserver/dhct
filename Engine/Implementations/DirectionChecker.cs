﻿using Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Implementations
{
    public class HorizontalChecker : IDirectionChecker
    {
        //TODO: Not ideal arguments, are showing implementation details, it would be better to encapsulate this
        public bool DirectionWon(IDictionary<int, List<ICell>> Cells, CellType cellType, int x, int y)
        {
            bool result = false;
            if(y == 0) {
                List<ICell> row = Cells[x];
                if (row[0].cellType == cellType && row[1].cellType == cellType)
                {
                    result = true;
                }
            }
            if (y == 1)
            {
                List<ICell> row = Cells[x];
                if (row[0].cellType == cellType && row[1].cellType == cellType)
                {
                    result = true;
                }
            }
            if (y == 2)
            {
                List<ICell> row = Cells[x];
                if (row[0].cellType == cellType && row[1].cellType == cellType)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
