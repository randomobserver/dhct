﻿using Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Implementations
{
    public class ResultChecker : IResultChecker
    {
        public bool Won()
        {
            IList<IDirectionChecker> checks = new List<IDirectionChecker>();
            checks.Add(new HorizontalChecker());
            return true;
        }
    }
}
