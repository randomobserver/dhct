﻿using Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Engine.Implementations
{
    public class Board : IBoard 
    {
        private IDictionary<int, List<ICell>> Cells;
        private IValidator validator;
        private IResultChecker resultChecker;
        private IUser[] Users;
        private bool user1Turn;

        public Board()
        {
            Cells = new Dictionary<int, List<ICell>>();
            Users = new User[2];
            validator = new Validator();
            resultChecker = new ResultChecker();
            user1Turn = true;
            InitializeUsers();
            InitializeBoard();
        }

        public bool Play()
        {
            bool result = false;
            while(CheckIfAllTaken())
            {
                if(user1Turn) {
                    Console.WriteLine("User 1 uses crosses. Please specify the coordinates starting from 0 with a space between x first e.g. 0 0");
                } else {
                    Console.WriteLine("User 2 uses noughts. Please specify the coordinates starting from 0 with a space between x first e.g. 0 0");
                }
                string[] coordinates = Console.ReadLine().Split(' ');
                int x, y;
                bool successx = Int32.TryParse(coordinates[0], out x);
                bool successy = Int32.TryParse(coordinates[1], out y);
                if(user1Turn == true)
                {
                    user1Turn = false;
                } else
                {
                    user1Turn = true;
                }

                if(successx && successy && validator.LegalStep(x,y,Cells[x][y].cellType)) {

                }
            }
            return result;
        }

        private void InitializeUsers()
        {
            Users[0].Name = "User1";
            Users[0].cellType = CellType.Cross;
            Users[1].Name = "User2";
            Users[1].cellType = CellType.Nought;
        }

        private void InitializeBoard()
        {
            List<ICell> row1 = new List<ICell>();
            row1.Add(new Cell() { x = 0, y = 0, cellType = CellType.Empty });
            row1.Add(new Cell() { x = 0, y = 1, cellType = CellType.Empty });
            row1.Add(new Cell() { x = 0, y = 2, cellType = CellType.Empty });

            List<ICell> row2 = new List<ICell>();
            row1.Add(new Cell() { x = 1, y = 0, cellType = CellType.Empty });
            row1.Add(new Cell() { x = 1, y = 1, cellType = CellType.Empty });
            row1.Add(new Cell() { x = 1, y = 2, cellType = CellType.Empty });

            List<ICell> row3 = new List<ICell>();
            row1.Add(new Cell() { x = 2, y = 0, cellType = CellType.Empty });
            row1.Add(new Cell() { x = 2, y = 1, cellType = CellType.Empty });
            row1.Add(new Cell() { x = 2, y = 2, cellType = CellType.Empty });

            Cells.Add(0, row1);
            Cells.Add(1, row2);
            Cells.Add(2, row3);

            
        }

        private void DisplayBoard()
        {
            foreach (List<ICell> row in Cells.Values) {
                char displayedCharacter1 = ConvertFromEnumToChar(row[0].cellType);
                char displayedCharacter2 = ConvertFromEnumToChar(row[1].cellType);
                char displayedCharacter3 = ConvertFromEnumToChar(row[2].cellType);
                Console.WriteLine("{0}  {1}  {2}", displayedCharacter1, displayedCharacter2, displayedCharacter3);
            }
        }

        private char ConvertFromEnumToChar(CellType cellType)
        {
            char result = '-';
            switch (cellType)
            {
                case CellType.Cross:
                    result = 'X';
                    break;
                case CellType.Nought:
                    result = 'O';
                    break;
            }

            return result;
        }

        private bool CheckIfAllTaken()
        {
            //ToDo: Check if there is any empty space left
            return false;
        }
    }
}
