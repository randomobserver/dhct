﻿using Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Implementations
{
    public class Validator : IValidator
    {
        public bool LegalStep(int x, int y, CellType cellType)
        {
            bool result = false;
            if(x>= 0 && x <=2 && y >= 0 && y <= 2 && cellType == CellType.Empty) {
                result = true;
            }
            return result;
        }
    }
}
