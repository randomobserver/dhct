﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Interfaces
{
    public interface IDirectionChecker
    {
        bool DirectionWon(IDictionary<int, List<ICell>> Cells, CellType cellType, int x, int y);
    }
}
