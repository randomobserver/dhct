﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Interfaces
{
    public interface IValidator
    {
        //ToDo: User is not needed here
        bool LegalStep(int x, int y, CellType cellType);
    }
}
