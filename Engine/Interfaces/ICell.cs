﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Interfaces
{
    public interface ICell
    {
        int x { get; set; }
        int y { get; set; }
        CellType cellType { get; set; }
    }
}
