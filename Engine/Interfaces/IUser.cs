﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Interfaces
{
    public interface IUser
    {
        string Name { get; set; }
        CellType cellType { get; set; }
    }
}
